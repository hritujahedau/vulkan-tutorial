#pragma once

#include <glm/glm.hpp>
#include <memory>
#include <random>
#include <vector>

#include "hv_game_object.hpp"

namespace hv
{
	class RainbowSystem
	{
	public:
		RainbowSystem(float flickerRate) : mFlickerRate{ flickerRate }
		{
			mColors = {
					{.8f, .1f, .1f},
					{.1f, .8f, .1f},
					{.1f, .1f, .8f},
					{.8f, .8f, .1f},
					{.8f, .1f, .8f},
					{.1f, .8f, .8f},
			};

			mElaspedTime = mFlickerRate;
		}

		void update(float dt, std::vector<hvGameObject>& gameObjects)
		{
			mElaspedTime -= dt;
			if (mElaspedTime < 0.f)
			{
				mElaspedTime += mFlickerRate;
				std::uniform_int_distribution<int> randInt{0, static_cast<int>(mColors.size()) - 1};
				for (auto& obj : gameObjects)
				{
					int randValue = randInt(mRng);
					obj.color = mColors[randValue];
				}
			}
		}

	private:
		std::random_device rd;
		std::mt19937 mRng{rd()};

		std::vector<glm::vec3> mColors;
		float mFlickerRate;
		float mElaspedTime;
	};
}
