#pragma once

#include "hv_model.hpp"

//libs
#include <glm/gtc/matrix_transform.hpp>
// std
#include <memory>
#include <unordered_map>

namespace hv
{
	struct TransformComponent
	{
		glm::vec3 translation{};
		glm::vec3 scale{1.0f, 1.0f, 1.0f};
		glm::vec3 rotation{};

		// Matrix correspond to translate * Ry * Rx * Rz * scale transformation 
		// Rotation conversion uses tait-bryan angles with axis order Y(1), X(2), Z(3) 
		glm::mat4 mat4();
		glm::mat3 normalMatrix();
	};

	struct PointLightComponent
	{
		float lightIntensity = 1.0f;
	};

	class hvGameObject
	{
	public:
		using id_t = unsigned int;
		using Map = std::unordered_map<id_t, hvGameObject>; 

		static hvGameObject createGameOBject()
		{
			static id_t currentId = 0;
			return hvGameObject(currentId++);
		}

		static hvGameObject makePointLight(float intensity = 1.0f, float radius = 0.1f, glm::vec3 color = glm::vec3(1.0f));

		hvGameObject(const hvGameObject&) = delete;
		hvGameObject& operator=(const hvGameObject&) = delete;

		hvGameObject(hvGameObject&&) = default;
		hvGameObject& operator=(hvGameObject&) = delete;

		id_t getId() {
			return id;
		}

		
		glm::vec3 color{};
		TransformComponent transform{};

		// optional pointer component
		std::shared_ptr<hvModel> model{};
		std::unique_ptr<PointLightComponent> pointLight = nullptr;

	private:
		hvGameObject(id_t objId) : id(objId) {}
		id_t id;

	};
}
