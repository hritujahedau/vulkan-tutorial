#pragma once

#include "hv_camera.hpp"
#include "hv_pipeline.hpp"
#include "hv_device.hpp"
#include "hv_game_object.hpp"
#include "hv_frame_info.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class PointLightSystem
	{
	public:

		PointLightSystem(hvDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalDescriptorSet);
		~PointLightSystem();

		PointLightSystem(const PointLightSystem&) = delete;
		PointLightSystem& operator=(const PointLightSystem) = delete;

		void update(FrameInfo& frameInfo, GlobalUbo& ubo);
		void render(FrameInfo& frameInfo);

	private:
		void createPipelineLayout(VkDescriptorSetLayout globalDescriptorSetLayout);
		void createPipeline(VkRenderPass renderPass);

		hvDevice& device;

		std::unique_ptr<hvPipeline> pipeline;
		VkPipelineLayout pipelineLayout;
	};
}
