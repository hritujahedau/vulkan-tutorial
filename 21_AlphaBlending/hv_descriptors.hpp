#pragma once

#include "hv_device.hpp"

// std
#include <memory>
#include <unordered_map>
#include <vector>

namespace hv {

    class hvDescriptorSetLayout {
    public:
        class Builder {
        public:
            Builder(hvDevice& device) : device{ device } {}

            Builder& addBinding(
                uint32_t binding,
                VkDescriptorType descriptorType,
                VkShaderStageFlags stageFlags,
                uint32_t count = 1);
            std::unique_ptr<hvDescriptorSetLayout> build() const;

        private:
            hvDevice& device;
            std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings{};
        };

        hvDescriptorSetLayout(
            hvDevice& device, std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings);
        ~hvDescriptorSetLayout();
        hvDescriptorSetLayout(const hvDescriptorSetLayout&) = delete;
        hvDescriptorSetLayout& operator=(const hvDescriptorSetLayout&) = delete;

        VkDescriptorSetLayout getDescriptorSetLayout() const {
            return descriptorSetLayout;
        }

    private:
        hvDevice& device;
        VkDescriptorSetLayout descriptorSetLayout;
        std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings;

        friend class hvDescriptorWriter;
    };

    class hvDescriptorPool {
    public:
        class Builder {
        public:
            Builder(hvDevice& device) : device{ device } {}

            Builder& addPoolSize(VkDescriptorType descriptorType, uint32_t count);
            Builder& setPoolFlags(VkDescriptorPoolCreateFlags flags);
            Builder& setMaxSets(uint32_t count);
            std::unique_ptr<hvDescriptorPool> build() const;

        private:
            hvDevice& device;
            std::vector<VkDescriptorPoolSize> poolSizes{};
            uint32_t maxSets = 1000;
            VkDescriptorPoolCreateFlags poolFlags = 0;
        };

        hvDescriptorPool(
            hvDevice& device,
            uint32_t maxSets,
            VkDescriptorPoolCreateFlags poolFlags,
            const std::vector<VkDescriptorPoolSize>& poolSizes);
        ~hvDescriptorPool();
        hvDescriptorPool(const hvDescriptorPool&) = delete;
        hvDescriptorPool& operator=(const hvDescriptorPool&) = delete;

        bool allocateDescriptor(
            const VkDescriptorSetLayout descriptorSetLayout, VkDescriptorSet& descriptor) const;

        void freeDescriptors(std::vector<VkDescriptorSet>& descriptors) const;

        void resetPool();

    private:
        hvDevice& device;
        VkDescriptorPool descriptorPool;

        friend class hvDescriptorWriter;
    };

    class hvDescriptorWriter {
    public:
        hvDescriptorWriter(hvDescriptorSetLayout& setLayout, hvDescriptorPool& pool);

        hvDescriptorWriter& writeBuffer(uint32_t binding, VkDescriptorBufferInfo* bufferInfo);
        hvDescriptorWriter& writeImage(uint32_t binding, VkDescriptorImageInfo* imageInfo);

        bool build(VkDescriptorSet& set);
        void overwrite(VkDescriptorSet& set);

    private:
        hvDescriptorSetLayout& setLayout;
        hvDescriptorPool& pool;
        std::vector<VkWriteDescriptorSet> writes;
    };
}