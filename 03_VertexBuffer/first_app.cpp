#include "first_app.hpp"

#include <stdexcept>
#include <array>

namespace hv
{
	void FirstApp::run()
	{
		while (!hvWindow.shouldClose())
		{
			glfwPollEvents();
			drawFrame();
		}
	}

	FirstApp::FirstApp()
	{
		loadModels();
		createPipelineLayout();
		createPipeline();
		createCommandBuffers();
	}

	FirstApp::~FirstApp()
	{
		vkDestroyPipelineLayout(device.device(), pipelineLayout, nullptr);
		//vkDestroyPipeline(device.device(), pipeline, nullptr);
	}

	void FirstApp::loadModels()
	{
		std::vector<hvModel::Vertex> vertices {
			{ {0.0, -0.5f},  {1.0, 0.0, 0.0} },
			{ {0.5f, 0.5f},  {0.0, 1.0, 0.0} },
			{ {-0.5f, 0.5f}, {0.0, 0.0, 1.0} }
		};

		model = std::make_unique<hvModel>(device, vertices);

	}

	void FirstApp::createPipelineLayout()
	{
		VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};

		pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutCreateInfo.setLayoutCount = 0;
		pipelineLayoutCreateInfo.pSetLayouts = nullptr;
		pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
		pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

		if (vkCreatePipelineLayout(device.device(), &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create pipeline layout");
		}
	}

	void FirstApp::createPipeline()
	{
		auto pipelineConfig = hvPipeline::defaultPiplelineConfig(swapChain.width(), swapChain.height());
		pipelineConfig.renderPass = swapChain.getRenderPass();
		pipelineConfig.pipelineLayout = pipelineLayout;
		pipeline = std::make_unique<hvPipeline>(
			device,
			"shader.vert.spv",
			"shader.frag.spv",
			pipelineConfig
		);
	}

	void FirstApp::createCommandBuffers()
	{
		commandbuffers.resize(swapChain.imageCount());

		VkCommandBufferAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = device.getCommandPool();
		allocInfo.commandBufferCount = static_cast<uint32_t>(swapChain.imageCount());

		if (vkAllocateCommandBuffers(device.device(), &allocInfo, commandbuffers.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to allocate command buffer");
		}

		for (int i = 0; i < commandbuffers.size(); i++)
		{
			VkCommandBufferBeginInfo begineInfo{};
			begineInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			if (vkBeginCommandBuffer(commandbuffers[i], &begineInfo) != VK_SUCCESS)
			{
				throw std::runtime_error("Failed to begin command buffer");
			}

			VkRenderPassBeginInfo renderPassInfo{};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = swapChain.getRenderPass();
			renderPassInfo.framebuffer = swapChain.getFrameBuffer(i);

			renderPassInfo.renderArea.offset = { 0,0 };
			renderPassInfo.renderArea.extent = swapChain.getSwapChainExtent();

			std::array<VkClearValue, 2> clearValues{};
			clearValues[0].color = { 0.1f, 0.1f, 0.1f, 1.0f };
			clearValues[1].depthStencil = { 1.0f, 0 };
			renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
			renderPassInfo.pClearValues = clearValues.data();

			vkCmdBeginRenderPass(commandbuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
			pipeline->bind(commandbuffers[i]);
			model->bind(commandbuffers[i]);
			model->draw(commandbuffers[i]);

			//vkCmdDraw(commandbuffers[i], 3, 1, 0, 0);
			//vkCmdEndRenderPass(commandbuffers[i]);
			if (vkEndCommandBuffer(commandbuffers[i]) != VK_SUCCESS)
			{
				std::runtime_error("Failed to record command buffer");
			}		
		}
	}

	void FirstApp::drawFrame()
	{
		uint32_t imageIndex;
		auto result = swapChain.acquireNextImage(&imageIndex);

		if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
		{
			throw std::runtime_error("Failed to aquire swap chain image");
		}

		result = swapChain.submitCommandBuffers(&commandbuffers[imageIndex], & imageIndex);
		if (result != VK_SUCCESS)
		{
			throw std::runtime_error("Faile to present swap chain image");
		}
	}

}
