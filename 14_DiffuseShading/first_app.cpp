#include "first_app.hpp"

#include "keyboard_movement.hpp"
#include "hv_camera.hpp"
#include "simple_render_system.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <stdexcept>
#include <array>
#include <cassert>
#include <chrono>

namespace hv
{
	FirstApp::FirstApp()
	{
		loadGameObjects();
	}

	FirstApp::~FirstApp()
	{
	}

	void FirstApp::run()
	{
		SimpleRenderSystem simpleRenderSystem{ device, renderer.getSwapChainRenderPass() };
		hvCamera camera{};

		auto viewObject = hvGameObject::createGameOBject();
		KeyboardMovementController cameraController;

		auto currentTime = std::chrono::high_resolution_clock::now();

		while (!window.shouldClose())
		{
			glfwPollEvents();
			
			auto newTime = std::chrono::high_resolution_clock::now();
			auto frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
			currentTime = newTime;

			//frameTime = glm::min(frameTime, MAX_FRAME_TIME);

			cameraController.moveInPlaneXz(window.getGLFWwindow(), frameTime, viewObject);
			camera.setViewYXZ(viewObject.transform.translation, viewObject.transform.rotation);

			float aspect = renderer.getAspectRatio();
			camera.setPerspectiveProjection(glm::radians(50.f), aspect, 0.1f, 10.0f);

			if (auto commandBuffer = renderer.beginFrame())
			{
				renderer.beginSwapChainRenderPass(commandBuffer);
				simpleRenderSystem.renderGameObjects(commandBuffer, gameObjects, camera);
				renderer.endSwapChainRenderPass(commandBuffer);
				renderer.endFrame();
			}
		}

		vkDeviceWaitIdle(device.device());
	}

	void FirstApp::loadGameObjects()
	{
		std::shared_ptr<hvModel> model = hvModel::createModelFromFile(device, "models/flat_vase.obj");

		auto flatVase = hvGameObject::createGameOBject();
		flatVase.model = model;
		flatVase.transform.translation = { -.5f, .5f, 2.5f };
		flatVase.transform.scale = glm::vec3{3.0f, 1.5f, 3.0f};
		gameObjects.push_back(std::move(flatVase));

		model = hvModel::createModelFromFile(device, "models/smooth_vase.obj");

		auto smoothVase = hvGameObject::createGameOBject();
		smoothVase.model = model;
		smoothVase.transform.translation = { .5f, .5f, 2.5f };
		smoothVase.transform.scale = glm::vec3{ 3.0f, 1.5f, 3.0f };
		gameObjects.push_back(std::move(smoothVase));
	}
}
