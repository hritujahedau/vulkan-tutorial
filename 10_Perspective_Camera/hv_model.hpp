#pragma once

#include "hv_device.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

// std
#include <vector>

namespace hv
{
	class hvModel
	{
	public:
	
		struct Vertex
		{
			glm::vec3 position;
			glm::vec3 color;
			static std::vector<VkVertexInputBindingDescription> getBindDescription();
			static std::vector<VkVertexInputAttributeDescription> getAttributeDescription();
		};

		hvModel(hvDevice &device, const std::vector<Vertex>& vertices);
		~hvModel();

		hvModel(const hvModel&) = delete;
		hvModel& operator=(const hvModel) = delete;

		void bind(VkCommandBuffer commandBuffer);
		void draw(VkCommandBuffer commandBuffer);

	private:
		void createVertexBuffer(const std::vector<Vertex>& vertices);

		hvDevice &device;
		VkBuffer vertexBuffer;
		VkDeviceMemory vertexBufferMemory;
		uint32_t vertexCount;

	};
}
