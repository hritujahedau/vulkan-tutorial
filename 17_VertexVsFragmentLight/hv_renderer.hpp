#pragma once

#include "hv_device.hpp"
#include "hv_swap_chain.hpp"
#include "hv_window.hpp"

// std
#include <memory>
#include <vector>
#include <cassert>

namespace hv
{
	class hvRenderer
	{
	public:

		hvRenderer(hvWindow& window, hvDevice& device);
		~hvRenderer();

		hvRenderer(const hvRenderer&) = delete;
		hvRenderer& operator=(const hvRenderer) = delete;

		VkRenderPass getSwapChainRenderPass() const
		{
			return swapChain->getRenderPass();
		}

		float getAspectRatio() const {
			return swapChain->extentAspectRatio();
		}

		bool isFrameinProgress() const
		{			
			return isFrameStarted;
		}

		VkCommandBuffer getCurrentCommandBuffer() const
		{
			assert(isFrameStarted && "Cannot get command buffer when frame is in progress");
			return commandbuffers[currentFrameIndex];
		}

		int getFrameIndex() const {
			assert(isFrameStarted && "Cannot get command buffer when frame is in progress");
			return currentFrameIndex;
		}

		VkCommandBuffer beginFrame();
		void endFrame();
		void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
		void endSwapChainRenderPass(VkCommandBuffer commandBuffer);


	private:
		void createCommandBuffers();
		void freeCommandBuffers();
		void drawFrame();
		void recreateSwapChain();
		 
		hvWindow &window;
		hvDevice& device;
		std::unique_ptr<hvSwapChain> swapChain;
		std::vector<VkCommandBuffer> commandbuffers;

		uint32_t currentImageIndex;
		int currentFrameIndex{ 0 };
		bool isFrameStarted{ false };
	};
}
