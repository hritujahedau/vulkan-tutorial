#pragma once

#include "hv_camera.hpp"
#include "hv_game_object.hpp"

// lib
#include <vulkan/vulkan.h>

namespace hv
{
	struct FrameInfo
	{
		int frameIndex;
		float frameTime;
		VkCommandBuffer commandBuffer;
		hvCamera camera;
		VkDescriptorSet globalDescriptorSet;
		hvGameObject::Map& gameObjects;
	};
}
