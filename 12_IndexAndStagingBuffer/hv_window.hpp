#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <string>

namespace hv
{
	class hvWindow
	{
	public:

		hvWindow(const hvWindow&) = delete;
		hvWindow& operator=(const hvWindow) = delete;

		hvWindow(int w, int h, std::string name);
		~hvWindow();
		bool shouldClose()
		{
			return glfwWindowShouldClose(window);
		}

		VkExtent2D getExtent() 
		{
			return { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
		}

		bool wasWindowResized() 
		{
			return framebufferResized;
		}

		void resetWindowResizedFlag()
		{
			framebufferResized = false;
		}

		GLFWwindow* getGLFWwindow() const {
			return window;
		}

		void createWindowSurface(VkInstance instance, VkSurfaceKHR* surf);

	private:
		static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
		void initWindow();
		int width;
		int height;
		bool framebufferResized = false;

		std::string windowName;
		GLFWwindow* window;

	};
}
