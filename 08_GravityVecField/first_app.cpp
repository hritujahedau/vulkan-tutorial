#include "first_app.hpp"

#include "simple_render_system.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <stdexcept>
#include <array>
#include <cassert>

namespace hv
{
	FirstApp::FirstApp()
	{
		loadGameObjects();
	}

	FirstApp::~FirstApp()
	{
	}

	void FirstApp::run()
	{
		SimpleRenderSystem simpleRenderSystem{ device, renderer.getSwapChainRenderPass() };
		while (!window.shouldClose())
		{
			glfwPollEvents();

			if (auto commandBuffer = renderer.beginFrame())
			{
				renderer.beginSwapChainRenderPass(commandBuffer);
				simpleRenderSystem.renderGameObjects(commandBuffer, gameObjects);
				renderer.endSwapChainRenderPass(commandBuffer);
				renderer.endFrame();
			}
		}
		vkDeviceWaitIdle(device.device());
	}

	void FirstApp::loadGameObjects()
	{
		std::vector<hvModel::Vertex> vertices {
			{ {0.0, -0.5f},  {1.0, 0.0, 0.0} },
			{ {0.5f, 0.5f},  {0.0, 1.0, 0.0} },
			{ {-0.5f, 0.5f}, {0.0, 0.0, 1.0} }
		};

		std::vector<glm::vec3> colors{
			{1.f, .7f, .73f},
			{ 1.f, .87f, .73f },
			{ 1.f, 1.f, .73f },
			{ .73f, 1.f, .8f },
			{ .73, .88f, 1.f }  
		};

		for (auto color : colors)
		{
			color = glm::pow(color, glm::vec3(2.2f));
		}

		auto model = std::make_shared<hvModel>(device, vertices);

		for (int i = 0; i < 1; i++)
		{
			auto triangle = hvGameObject::createGameOBject();
			triangle.model = model;
			triangle.color = colors[i % colors.size()];
			triangle.transform2d.scale = glm::vec2(.5f) + i * 0.025f;
			triangle.transform2d.rotation = i * glm::pi<float>() * 0.25f;

			gameObjects.push_back(std::move(triangle));
		}
	}
}
