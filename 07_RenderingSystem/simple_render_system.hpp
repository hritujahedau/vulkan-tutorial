#pragma once

#include "hv_pipeline.hpp"
#include "hv_device.hpp"
#include "hv_game_object.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class SimpleRenderSystem
	{
	public:
		
		SimpleRenderSystem(hvDevice &device, VkRenderPass renderPass);
		~SimpleRenderSystem();

		SimpleRenderSystem(const SimpleRenderSystem&) = delete;
		SimpleRenderSystem& operator=(const SimpleRenderSystem) = delete;

		void renderGameObjects(VkCommandBuffer commandBuffer, std::vector<hvGameObject>& gameObjects);

	private:
		void loadGameObjects();
		void createPipelineLayout();
		void createPipeline(VkRenderPass renderPass);
		
		hvDevice& device;
		
		std::unique_ptr<hvPipeline> pipeline;
		VkPipelineLayout pipelineLayout;
	};
}
