#include "hv_model.hpp"

// std
#include <assert.h>

namespace hv
{
	hvModel::hvModel(hvDevice &device_, const std::vector<Vertex>& vertices) : device(device_)
	{
		createVertexBuffer(vertices);
	}
	
	hvModel::~hvModel()
	{
		vkDestroyBuffer(device.device(), vertexBuffer, nullptr);
		vkFreeMemory(device.device(), vertexBufferMemory, nullptr);
	}

	void hvModel::createVertexBuffer(const std::vector<Vertex>& vertices)
	{
		vertexCount = static_cast<uint32_t>(vertices.size());
		assert(vertexCount >= 3 && "Vertex count must be atleast 3");
		VkDeviceSize bufferSize = sizeof(vertices[0]) * vertexCount;

		device.createBuffer(bufferSize,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			vertexBuffer,
			vertexBufferMemory
			);

		void* data;
		vkMapMemory(device.device(), vertexBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, vertices.data(), static_cast<size_t>(bufferSize));
		vkUnmapMemory(device.device(), vertexBufferMemory);
	}

	void hvModel::draw(VkCommandBuffer commandBuffer)
	{
		vkCmdDraw(commandBuffer, vertexCount, 1, 0, 0);
	}

	void hvModel::bind(VkCommandBuffer commandBuffer)
	{
		VkBuffer buffers[] = { vertexBuffer };
		VkDeviceSize offset[] = { 0 };
		vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offset);
	}

	std::vector<VkVertexInputBindingDescription> hvModel::Vertex::getBindDescription()
	{
		std::vector< VkVertexInputBindingDescription> bindingDescriptor(1);
		bindingDescriptor[0].binding = 0;
		bindingDescriptor[0].stride = sizeof(Vertex);
		bindingDescriptor[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		return bindingDescriptor;
	}

	std::vector<VkVertexInputAttributeDescription> hvModel::Vertex::getAttributeDescription()
	{
		std::vector<VkVertexInputAttributeDescription> attributeDescriptor(2);
		attributeDescriptor[0].binding = 0;
		attributeDescriptor[0].location = 0;
		attributeDescriptor[0].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptor[0].offset = offsetof(Vertex, position);

		attributeDescriptor[1].binding = 0;
		attributeDescriptor[1].location = 1;
		attributeDescriptor[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptor[1].offset = offsetof(Vertex, color);

		return attributeDescriptor;
	}
}
