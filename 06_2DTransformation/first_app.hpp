#pragma once

#include "hv_window.hpp"
#include "hv_pipeline.hpp"
#include "hv_device.hpp"
#include "hv_swap_chain.hpp"
#include "hv_game_object.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class FirstApp
	{
	public:
		static constexpr int WIDTH = 800;
		static constexpr int HEIGHT = 600;
		void run();

		FirstApp();
		~FirstApp();

		FirstApp(const FirstApp&) = delete;
		FirstApp& operator=(const FirstApp) = delete;

	private:
		void loadGameObjects();
		void createPipelineLayout();
		void createPipeline();
		void createCommandBuffers();
		void freeCommandBuffers();
		void drawFrame();
		void recreateSwapChain();
		void recordCommandBuffer(int imageIndex);
		void renderGameObjects(VkCommandBuffer commandBuffer);

		hvWindow hvWindow{ WIDTH, HEIGHT, "Hello Vulkan" };
		hvDevice device{ hvWindow };
		std::unique_ptr<hvSwapChain> swapChain;
		std::unique_ptr<hvPipeline> pipeline;
		VkPipelineLayout pipelineLayout;
		std::vector<VkCommandBuffer> commandbuffers;
		std::vector<hvGameObject> gameObject;
	};
}
