#include "first_app.hpp"

#include <stdexcept>
#include <array>

namespace hv
{
	void FirstApp::run()
	{
		while (!hvWindow.shouldClose())
		{
			glfwPollEvents();
			drawFrame();
		}
	}

	FirstApp::FirstApp()
	{
		loadModels();
		createPipelineLayout();
		recreateSwapChain();
		createCommandBuffers();
	}

	FirstApp::~FirstApp()
	{
		vkDestroyPipelineLayout(device.device(), pipelineLayout, nullptr);
	}

	void FirstApp::loadModels()
	{
		std::vector<hvModel::Vertex> vertices {
			{ {0.0, -0.5f},  {1.0, 0.0, 0.0} },
			{ {0.5f, 0.5f},  {0.0, 1.0, 0.0} },
			{ {-0.5f, 0.5f}, {0.0, 0.0, 1.0} }
		};

		model = std::make_unique<hvModel>(device, vertices);

	}

	void FirstApp::createPipelineLayout()
	{
		VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};

		pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutCreateInfo.setLayoutCount = 0;
		pipelineLayoutCreateInfo.pSetLayouts = nullptr;
		pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
		pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

		if (vkCreatePipelineLayout(device.device(), &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create pipeline layout");
		}
	}

	void FirstApp::createPipeline()
	{
		PipelineConfigInfo pipelineConfig{};
		hvPipeline::defaultPiplelineConfig(pipelineConfig);
		pipelineConfig.renderPass = swapChain->getRenderPass();
		pipelineConfig.pipelineLayout = pipelineLayout;
		pipeline = std::make_unique<hvPipeline>(
			device,
			"shader.vert.spv",
			"shader.frag.spv",
			pipelineConfig
		);
	}

	void FirstApp::recreateSwapChain()
	{
		auto extent = hvWindow.getExtent();
		while (extent.width == 0 || extent.height == 0)
		{
			extent = hvWindow.getExtent();
			glfwWaitEvents();
		}

		vkDeviceWaitIdle(device.device());

		if (swapChain == nullptr)
		{
			swapChain = std::make_unique<hvSwapChain>(device, extent);
		}
		else
		{
			swapChain = std::make_unique<hvSwapChain>(device, extent, std::move(swapChain));
			if (swapChain->imageCount() != commandbuffers.size())
			{
				freeCommandBuffers();
				createCommandBuffers();
			}
		}
		createPipeline();
	}

	void FirstApp::createCommandBuffers()
	{
		commandbuffers.resize(swapChain->imageCount());

		VkCommandBufferAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = device.getCommandPool();
		allocInfo.commandBufferCount = static_cast<uint32_t>(swapChain->imageCount());

		if (vkAllocateCommandBuffers(device.device(), &allocInfo, commandbuffers.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to allocate command buffer");
		}

	}

	void FirstApp::freeCommandBuffers()
	{
		vkFreeCommandBuffers(
			device.device(),
			device.getCommandPool(),
			static_cast<float>(commandbuffers.size()),
			commandbuffers.data());
		commandbuffers.clear();
	}

	void FirstApp::recordCommandBuffer(int imageIndex)
	{
		VkCommandBufferBeginInfo begineInfo{};
		begineInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		if (vkBeginCommandBuffer(commandbuffers[imageIndex], &begineInfo) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to begin command buffer");
		}

		VkRenderPassBeginInfo renderPassInfo{};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = swapChain->getRenderPass();
		renderPassInfo.framebuffer = swapChain->getFrameBuffer(imageIndex);

		renderPassInfo.renderArea.offset = { 0,0 };
		renderPassInfo.renderArea.extent = swapChain->getSwapChainExtent();

		std::array<VkClearValue, 2> clearValues{};
		clearValues[0].color = { 0.1f, 0.1f, 0.1f, 1.0f };
		clearValues[1].depthStencil = { 1.0f, 0 };
		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		vkCmdBeginRenderPass(commandbuffers[imageIndex], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
		
		VkViewport viewport;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = static_cast<float>(swapChain->getSwapChainExtent().width);
		viewport.height = static_cast<float>(swapChain->getSwapChainExtent().height);
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		VkRect2D scissor{ {0,0}, swapChain->getSwapChainExtent() };
		vkCmdSetViewport(commandbuffers[imageIndex], 0, 1, &viewport);
		vkCmdSetScissor(commandbuffers[imageIndex], 0, 1, &scissor);

		pipeline->bind(commandbuffers[imageIndex]);
		model->bind(commandbuffers[imageIndex]);
		model->draw(commandbuffers[imageIndex]);
		
		if (vkEndCommandBuffer(commandbuffers[imageIndex]) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to record command buffer");
		}
	}

	void FirstApp::drawFrame()
	{
		uint32_t imageIndex;
		auto result = swapChain->acquireNextImage(&imageIndex);

		if (result == VK_ERROR_OUT_OF_DATE_KHR)
		{
			recreateSwapChain();
			return;
		}

		if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
		{
			throw std::runtime_error("Failed to aquire swap chain image");
		}

		recordCommandBuffer(imageIndex);
		result = swapChain->submitCommandBuffers(&commandbuffers[imageIndex], & imageIndex);
		if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || hvWindow.wasWindowResized())
		{
			hvWindow.resetWindowResizedFlag();
			recreateSwapChain();
			return;
		}
		if (result != VK_SUCCESS)
		{
			throw std::runtime_error("Faile to present swap chain image");
		}
	}

}
