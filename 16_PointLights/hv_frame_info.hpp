#pragma once

#include "hv_camera.hpp"

// lib
#include <vulkan/vulkan.h>

namespace hv
{
	struct FrameInfo
	{
		int frameIndex;
		float frameTime;
		VkCommandBuffer commandBuffer;
		hvCamera camera;
		VkDescriptorSet globalDescriptorSet;
	};
}
