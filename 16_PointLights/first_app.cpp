#include "first_app.hpp"

#include "keyboard_movement.hpp"
#include "hv_camera.hpp"
#include "simple_render_system.hpp"
#include "hv_buffer.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <stdexcept>
#include <array>
#include <cassert>
#include <chrono>

namespace hv
{
	struct GlobalUbo
	{
		glm::mat4 projectionView {1.0f};
		glm::vec4 ambientLightColor{1.0f, 1.0f, 1.0f, 0.02f}; // w is intensity
		glm::vec3 lightPosition{-1.0f};
		alignas(16) glm::vec4 lightColor{1.0f};
	};

	FirstApp::FirstApp()
	{
		globalPool = hvDescriptorPool::Builder(device)
			.setMaxSets(hvSwapChain::MAX_FRAMES_IN_FLIGHT)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, hvSwapChain::MAX_FRAMES_IN_FLIGHT)
			.build();

		loadGameObjects();
	}

	FirstApp::~FirstApp()
	{
	}

	void FirstApp::run()
	{
		std::vector<std::unique_ptr<hvBuffer>> uboBuffers(hvSwapChain::MAX_FRAMES_IN_FLIGHT);
		for (int i = 0; i < uboBuffers.size(); i++)
		{
			uboBuffers[i] = std::make_unique<hvBuffer>(
				device,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
			uboBuffers[i]->map();
		}		

		auto globalSetLayout = hvDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
			.build();

		std::vector<VkDescriptorSet> globalDescriptorSets(hvSwapChain::MAX_FRAMES_IN_FLIGHT);
		for (int i = 0; i < globalDescriptorSets.size(); i++)
		{
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			hvDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}

		SimpleRenderSystem simpleRenderSystem{ device, renderer.getSwapChainRenderPass(),
		globalSetLayout->getDescriptorSetLayout()};
		hvCamera camera{};

		auto viewObject = hvGameObject::createGameOBject();
		viewObject.transform.translation.z = -2.5f;
		KeyboardMovementController cameraController;

		auto currentTime = std::chrono::high_resolution_clock::now();

		while (!window.shouldClose())
		{
			glfwPollEvents();
			
			auto newTime = std::chrono::high_resolution_clock::now();
			auto frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
			currentTime = newTime;

			//frameTime = glm::min(frameTime, MAX_FRAME_TIME);

			cameraController.moveInPlaneXz(window.getGLFWwindow(), frameTime, viewObject);
			camera.setViewYXZ(viewObject.transform.translation, viewObject.transform.rotation);

			float aspect = renderer.getAspectRatio();
			camera.setPerspectiveProjection(glm::radians(50.f), aspect, 0.1f, 1000.0f);

			if (auto commandBuffer = renderer.beginFrame())
			{
				int frameIndex = renderer.getFrameIndex();
				FrameInfo frameInfo{
					frameIndex,
					frameTime,
					commandBuffer,
					camera,
					globalDescriptorSets[frameIndex]
				};

				// update
				GlobalUbo ubo{};
				ubo.projectionView = frameInfo.camera.getProjection() * frameInfo.camera.getView();
				uboBuffers[frameIndex]->writeToBuffer(&ubo);
				uboBuffers[frameIndex]->flush();

				// render
				renderer.beginSwapChainRenderPass(commandBuffer);
				simpleRenderSystem.renderGameObjects(frameInfo, gameObjects, frameInfo.camera);
				renderer.endSwapChainRenderPass(commandBuffer);
				renderer.endFrame();
			}
		}
		vkDeviceWaitIdle(device.device());
	}

	void FirstApp::loadGameObjects()
	{
		std::shared_ptr<hvModel> model = hvModel::createModelFromFile(device, "models/flat_vase.obj");

		auto flatVase = hvGameObject::createGameOBject();
		flatVase.model = model;
		flatVase.transform.translation = { -.5f, .5f, .0f };
		flatVase.transform.scale = glm::vec3{3.0f, 1.5f, 3.0f};
		gameObjects.push_back(std::move(flatVase));

		model = hvModel::createModelFromFile(device, "models/smooth_vase.obj");

		auto smoothVase = hvGameObject::createGameOBject();
		smoothVase.model = model;
		smoothVase.transform.translation = { .5f, .5f, .0f };
		smoothVase.transform.scale = glm::vec3{ 3.0f, 1.5f, 3.0f };
		gameObjects.push_back(std::move(smoothVase));

		model = hvModel::createModelFromFile(device, "models/quad.obj");

		auto floor = hvGameObject::createGameOBject();
		floor.model = model;
		floor.transform.translation = { .0f, .5f, .0f };
		floor.transform.scale = glm::vec3{ 3.0f, 1.0f, 3.0f };
		gameObjects.push_back(std::move(floor));
	}
}
