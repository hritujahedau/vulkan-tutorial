#pragma once

#include "hv_window.hpp"
#include "hv_device.hpp"
#include "hv_game_object.hpp"
#include "hv_renderer.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class FirstApp
	{
	public:
		static constexpr int WIDTH = 800;
		static constexpr int HEIGHT = 600;
		void run();

		FirstApp();
		~FirstApp();

		FirstApp(const FirstApp&) = delete;
		FirstApp& operator=(const FirstApp) = delete;

	private:
		void loadGameObjects();

		hvWindow window{ WIDTH, HEIGHT, "Hello Vulkan" };
		hvDevice device{ window };
		hvRenderer renderer{ window, device };

		std::vector<hvGameObject> gameObjects;
	};
}
