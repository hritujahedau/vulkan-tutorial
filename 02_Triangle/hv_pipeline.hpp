#pragma once

#include "hv_device.hpp"

// std
#include <string>
#include <vector>

namespace hv
{
	struct PipelineConfigInfo
	{
		VkViewport viewport{};
		VkRect2D scissor{};
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
		VkPipelineRasterizationStateCreateInfo rasterizationInfo{};
		VkPipelineMultisampleStateCreateInfo multisampleInfo{};
		VkPipelineColorBlendAttachmentState colorBlendAttach{};
		VkPipelineColorBlendStateCreateInfo colorBlendInfo{};
		VkPipelineDepthStencilStateCreateInfo depthStencilInfo{};
		VkPipelineLayout pipelineLayout = nullptr;
		VkRenderPass renderPass = nullptr;
		uint32_t subpass = 0;
	};

	class hvPipeline
	{
	public:
		hvPipeline(hvDevice &device, const std::string &vertexShader, 
			const std::string &fragmentshader, const PipelineConfigInfo configInfo);

		~hvPipeline();
		hvPipeline(const hvPipeline&) = delete;
		void operator=(const hvPipeline&) = delete;

		void bind(VkCommandBuffer commandBuffer);
		static PipelineConfigInfo defaultPiplelineConfig(uint32_t width, uint32_t height);
	private:
		static std::vector<char> readFile(const std::string& filename);
		void createGraphicsPipeline(const std::string& vertexShader, 
			const std::string& fragmentShader,
			const PipelineConfigInfo configInfo);

		void createShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);

		hvDevice& device;
		VkPipeline graphicsPipeline;
		VkShaderModule vertShaderModule;
		VkShaderModule fragShaderModule;
	};
}


