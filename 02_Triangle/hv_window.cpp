#include "hv_window.hpp"

// std
#include <stdexcept>

namespace hv
{
	hvWindow::hvWindow(int w, int h, std::string name) :
		width(w),
		height(h),
		windowName(name)
	{
		initWindow();
	}

	hvWindow::~hvWindow()
	{
		glfwDestroyWindow(window);
		glfwTerminate();
	}

	void hvWindow::initWindow()
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
	}

	void hvWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR* surf)
	{
		if (glfwCreateWindowSurface(instance, window, nullptr, surf) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create window surface");
		}
	}
}
