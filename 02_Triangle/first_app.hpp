#pragma once

#include "hv_window.hpp"
#include "hv_pipeline.hpp"
#include "hv_device.hpp"
#include "hv_swap_chain.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class FirstApp
	{
	public:
		static constexpr int WIDTH = 800;
		static constexpr int HEIGHT = 600;
		void run();

		FirstApp();
		~FirstApp();

		FirstApp(const FirstApp&) = delete;
		FirstApp& operator=(const FirstApp) = delete;

	private:
		void createPipelineLayout();
		void createPipeline();
		void createCommandBuffers();
		void drawFrame();

		hvWindow hvWindow{ WIDTH, HEIGHT, "Hello Vulkan" };
		hvDevice device{ hvWindow };
		hvSwapChain swapChain{ device, hvWindow.getExtent() };
		/*hvPipeline pipeline{device, "shader.vert.spv","shader.frag.spv",
			hvPipeline::defaultPiplelineConfig(WIDTH, HEIGHT)};*/
		std::unique_ptr<hvPipeline> pipeline;
		VkPipelineLayout pipelineLayout;
		std::vector<VkCommandBuffer> commandbuffers;
	};
}
