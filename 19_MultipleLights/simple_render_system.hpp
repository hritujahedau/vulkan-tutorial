#pragma once

#include "hv_camera.hpp"
#include "hv_pipeline.hpp"
#include "hv_device.hpp"
#include "hv_game_object.hpp"
#include "hv_frame_info.hpp"

// std
#include <memory>
#include <vector>

namespace hv
{
	class SimpleRenderSystem
	{
	public:
		
		SimpleRenderSystem(hvDevice &device, VkRenderPass renderPass, VkDescriptorSetLayout globalDescriptorSet);
		~SimpleRenderSystem();

		SimpleRenderSystem(const SimpleRenderSystem&) = delete;
		SimpleRenderSystem& operator=(const SimpleRenderSystem) = delete;

		void renderGameObjects(FrameInfo& frameInfo);

	private:
		void createPipelineLayout(VkDescriptorSetLayout globalDescriptorSetLayout);
		void createPipeline(VkRenderPass renderPass);
		
		hvDevice& device;
		
		std::unique_ptr<hvPipeline> pipeline;
		VkPipelineLayout pipelineLayout;
	};
}
