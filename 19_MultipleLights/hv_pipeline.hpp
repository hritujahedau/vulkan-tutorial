#pragma once

#include "hv_device.hpp"

// std
#include <string>
#include <vector>

namespace hv
{
	struct PipelineConfigInfo
	{
		// PipelineConfigInfo(const PipelineConfigInfo&) = delete;
		// PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;

		std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

		VkPipelineViewportStateCreateInfo viewportInfo;
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
		VkPipelineRasterizationStateCreateInfo rasterizationInfo{};
		VkPipelineMultisampleStateCreateInfo multisampleInfo{};
		VkPipelineColorBlendAttachmentState colorBlendAttach{};
		VkPipelineColorBlendStateCreateInfo colorBlendInfo{};
		VkPipelineDepthStencilStateCreateInfo depthStencilInfo{};
		std::vector<VkDynamicState> dynamicStateEnables;
		VkPipelineDynamicStateCreateInfo dynamicStateInfo;
		VkPipelineLayout pipelineLayout = nullptr;
		VkRenderPass renderPass = nullptr;
		uint32_t subpass = 0;
	};

	class hvPipeline
	{
	public:
		hvPipeline(hvDevice &device, const std::string &vertexShader, 
			const std::string &fragmentshader, const PipelineConfigInfo configInfo);

		~hvPipeline();
		hvPipeline(const hvPipeline&) = delete;
		hvPipeline operator=(const hvPipeline&) = delete;

		void bind(VkCommandBuffer commandBuffer);
		static void defaultPipelineConfig(PipelineConfigInfo& configInfo);
	private:
		static std::vector<char> readFile(const std::string& filename);
		void createGraphicsPipeline(const std::string& vertexShader, 
			const std::string& fragmentShader,
			const PipelineConfigInfo configInfo);

		void createShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);

		hvDevice& device;
		VkPipeline graphicsPipeline;
		VkShaderModule vertShaderModule;
		VkShaderModule fragShaderModule;
	};
}


