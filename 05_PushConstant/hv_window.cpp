#include "hv_window.hpp"

// std
#include <stdexcept>

namespace hv
{
	hvWindow::hvWindow(int w, int h, std::string name) :
		width(w),
		height(h),
		windowName(name)
	{
		initWindow();
	}

	hvWindow::~hvWindow()
	{
		glfwDestroyWindow(window);
		glfwTerminate();
	}

	void hvWindow::initWindow()
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
		glfwSetWindowUserPointer(window, this);
		glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	}

	void hvWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR* surf)
	{
		if (glfwCreateWindowSurface(instance, window, nullptr, surf) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create window surface");
		}
	}

	void hvWindow::framebufferResizeCallback(GLFWwindow* window, int width, int height)
	{
		auto hwindow = reinterpret_cast<hvWindow*>(glfwGetWindowUserPointer(window));
		hwindow->framebufferResized = true;
		hwindow->width = width;
		hwindow->height = height;

	}
}
